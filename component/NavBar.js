import Link from 'next/link';

export default function NavBar(){
    return(
        <header className="header">
            <nav className="navbar navbar-expand-lg fixed-top">
                <div className="container">
                    <Link href="/">
                        <a className="navbar-brand">Ismael.</a>
                    </Link>
                <button type="button" data-toggle="collapse" data-target="#navbarcollapse" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation" className="navbar-toggler navbar-toggler-right"><span className="fa fa-bars"></span></button>
                <div id="navbarcollapse" className="collapse navbar-collapse">
                    <ul className="navbar-nav ml-auto">
                        <li className="nav-item">
                            <Link href="/">                                
                            <a className="nav-link">Home</a>
                            </Link>                                                        
                        </li>
                        <li className="nav-item">
                            <Link href="/about">
                            <a className="nav-link">About</a>
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link href="/skills">
                            <a className="nav-link">Skills</a>
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link href="/project">
                            <a className="nav-link">Projects</a>
                            </Link>
                        </li>
                        <li className="nav-item">
                            <Link href="/contact">
                            <a className="nav-link">Contact</a>
                            </Link>
                        </li>
                    </ul>
                </div>
                </div>
            </nav>
        </header>
      )
}