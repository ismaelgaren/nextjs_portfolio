import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFacebookF } from '@fortawesome/free-brands-svg-icons';
import { faLinkedinIn } from '@fortawesome/free-brands-svg-icons';
import { faInstagram } from '@fortawesome/free-brands-svg-icons';
import { faGoogle } from '@fortawesome/free-brands-svg-icons';
import { faMapMarkerAlt } from '@fortawesome/free-solid-svg-icons';
import { faMobile } from '@fortawesome/free-solid-svg-icons';
import { faEnvelope } from '@fortawesome/free-solid-svg-icons';
import Link from 'next/link';

export default function Contact(){

    const facebook = <FontAwesomeIcon icon={faFacebookF} />
	const linkedIn = <FontAwesomeIcon icon={faLinkedinIn} />
	const instagram = <FontAwesomeIcon icon={faInstagram}/>
    const gmail = <FontAwesomeIcon icon={faGoogle}/>
    const mapMarker = <FontAwesomeIcon icon={faMapMarkerAlt} size="2x"/>
	const mobile = <FontAwesomeIcon icon={faMobile} size="2x" />
	const envelope = <FontAwesomeIcon icon={faEnvelope} size="2x" />

    return(
        <>
        <section className="bg-light mb-4" id="contact">
	      <div className="container">
            <header class="text-center">
                <h2 class="title">Contact me</h2>
            </header>
	        <div className="row">
	          <div className="col-lg-3 col-md-6 mb-4 mb-lg-0">
	            <div className="px-4 py-5 text-center contact-item shadow-sm">{mapMarker}
	              <h4 className="contact-item-title h5 text-uppercase">Location</h4>
	              <p className="text-small mb-0">Malabon, Metro Manila</p>
	            </div>
	          </div>
	          <div className="col-lg-3 col-md-6 mb-4 mb-lg-0">
	            <div className="px-4 py-5 text-center contact-item shadow-sm">{mobile}
	              <h4 className="contact-item-title h5 text-uppercase">Phone</h4>
	              <p className="text-small mb-0">0945 277 7896</p>
	            </div>
	          </div>
	          <div className="col-lg-3 col-md-6 mb-4 mb-lg-0"><a className="px-4 py-5 text-center contact-item shadow-sm d-block reset-anchor" href="https://www.linkedin.com/in/ismael-garen-3377711b9/">{linkedIn}
	              <h4 className="contact-item-title h5 text-uppercase">Linked in</h4>
	              <p className="text-small mb-0">@ismaelgaren</p></a></div>
	          <div className="col-lg-3 col-md-6 mb-4 mb-lg-0"><a className="px-4 py-5 text-center contact-item shadow-sm d-block reset-anchor" href="https://mail.google.com/mail/u/0/#inbox">{envelope}
	              <h4 className="contact-item-title h5 text-uppercase">Email</h4>
	              <p className="text-small mb-0">jovygar1@gmail.com</p></a></div>
	        </div>
	      </div>
	    </section>

        <footer className="main-footer">
            <div className="container">
            <div className="row">
                <div className="col-md-6 text-center text-lg-left">
                <p className="social">
                    <Link href="https://www.facebook.com/jovygaren/">  
                    <a className="external facebook">{facebook}</a>
                    </Link>
                    <Link href="https://www.instagram.com/ismaelgrn/">
                    <a className="external instagram">{instagram}</a>
                    </Link>
                    <Link href="https://www.gmail.com">
                    <a className="external gplus">{gmail}</a>
                    </Link>
                    <Link href="https://www.linkedin.com/in/ismael-garen-3377711b9/">
                    <a className="external email">{linkedIn}</a>
                    </Link>
                </p>
                </div>

                <div className="col-md-6 text-center text-lg-right mt-4 mt-lg-0">
                <p>© 2021 All rights reserved.</p>
                </div>
                <div className="col-12 mt-4">
                <p className="template-bootstrapious">Template by <a href='https://www.facebook.com/jovygaren/'>Ismael Garen</a></p>
        
                </div>
            </div>
            </div>
        </footer>
        </>
    )
}