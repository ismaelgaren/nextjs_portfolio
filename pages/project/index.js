import Link from 'next/link'
import cs2 from '../../images/booking_system_cs2.jpg';
import firstPortfolio from '../../images/firstportfolio.jpg';
import joes from '../../images/joes.jpg';
import reactPortfolio from '../../images/reactPortfolio.jpg'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFacebookF } from '@fortawesome/free-brands-svg-icons';
import { faLinkedinIn } from '@fortawesome/free-brands-svg-icons';
import { faInstagram } from '@fortawesome/free-brands-svg-icons';
import { faGoogle } from '@fortawesome/free-brands-svg-icons';

export default function Projects(){

    const facebook = <FontAwesomeIcon icon={faFacebookF} />
	const linkedIn = <FontAwesomeIcon icon={faLinkedinIn} />
	const instagram = <FontAwesomeIcon icon={faInstagram}/>
    const gmail = <FontAwesomeIcon icon={faGoogle}/>
    
    return(
        <>
        <section id="references">
            <div className="container">
                <div className="col-sm-12">
                <div className="mb-5 text-center">
                    <h2 className="title">My Projects</h2>
                    <p className="lead">I have worked on dozens of projects so I have picked only the latest for you.</p>
                </div>
                <ul id="filter">
                    <li><a href="#" data-filter="webdesign">Web Development</a></li>
                </ul>
                <div id="detail">
                    <div className="row">
                    <div className="col-lg-10 mx-auto"><span className="close">×</span>
                        <div id="detail-slider" className="owl-carousel owl-theme"></div>
                        <div className="text-center">
                        <h1 id="detail-title" className="title"></h1>
                        </div>
                        <div id="detail-content"></div>
                    </div>
                    </div>
                </div>
                <div id="references-masonry">
                    <div className="row">
                        <div data-category="webdesign" className="reference-item col-lg-3 col-md-6">
                            <div className="reference">
                                <Link href="https://ismaelgaren.gitlab.io/cs2_client_public/">
                                <a>
                                    <img src={cs2} className="img-fluid" />
                                <div className="overlay">
                                    <div className="inner">
                                    <h3 className="h4 reference-title">Course Booking System</h3>
                                    <p>Capstone 2</p>
                                    </div>
                                </div></a>
                                </Link>
                            </div>
                        </div>
                        <div data-category="webdesign" className="reference-item col-lg-3 col-md-6">
                            <div className="reference">
                                <Link href="https://ismaelgaren.gitlab.io/portfolio/index.html">
                                <a>
                                    <img src={firstPortfolio} className="img-fluid" />
                                <div className="overlay">
                                    <div className="inner">
                                    <h3 className="h4 reference-title">First Portfolio</h3>
                                    <p>Simple Portfolio</p>
                                    </div>
                                </div></a>
                                </Link>
                            </div>
                        </div>
                        <div data-category="webdesign" className="reference-item col-lg-3 col-md-6">
                            <div className="reference">
                                <Link href="https://joes-navy.vercel.app/">
                                <a>
                                    <img src={joes} className="img-fluid" />
                                <div className="overlay">
                                    <div className="inner">
                                    <h3 className="h4 reference-title">JO-ES Publishing House Inc</h3>
                                    <p>Company Website</p>
                                    </div>
                                </div></a>
                                </Link>
                            </div>
                        </div>
                        <div data-category="webdesign" className="reference-item col-lg-3 col-md-6">
                            <div className="reference">
                                <Link href="https://ismael-react-portfolio.vercel.app/">
                                <a>
                                    <img src={reactPortfolio} className="img-fluid" />
                                <div className="overlay">
                                    <div className="inner">
                                    <h3 className="h4 reference-title">React Portfolio</h3>
                                    <p>Mini Capstone</p>
                                    </div>
                                </div></a>
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </section>

        <footer className="main-footer">
            <div className="container">
            <div className="row">
                <div className="col-md-6 text-center text-lg-left">
                <p className="social">
                    <Link href="https://www.facebook.com/jovygaren/">  
                    <a className="external facebook">{facebook}</a>
                    </Link>
                    <Link href="https://www.instagram.com/ismaelgrn/">
                    <a className="external instagram">{instagram}</a>
                    </Link>
                    <Link href="https://www.gmail.com">
                    <a className="external gplus">{gmail}</a>
                    </Link>
                    <Link href="https://www.linkedin.com/in/ismael-garen-3377711b9/">
                    <a className="external email">{linkedIn}</a>
                    </Link>
                </p>
                </div>

                <div className="col-md-6 text-center text-lg-right mt-4 mt-lg-0">
                <p>© 2021 All rights reserved.</p>
                </div>
                <div className="col-12 mt-4">
                <p className="template-bootstrapious">Template by <a href='https://www.facebook.com/jovygaren/'>Ismael Garen</a></p>
        
                </div>
            </div>
            </div>
        </footer>
      </>
    )
}