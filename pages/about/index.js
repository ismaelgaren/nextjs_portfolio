const Me = require('../../images/me.jpg')
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFacebookF } from '@fortawesome/free-brands-svg-icons';
import { faLinkedinIn } from '@fortawesome/free-brands-svg-icons';
import { faInstagram } from '@fortawesome/free-brands-svg-icons';
import { faGoogle } from '@fortawesome/free-brands-svg-icons';
import Link from 'next/link'

export default function About(){
    const facebook = <FontAwesomeIcon icon={faFacebookF} />
	const linkedIn = <FontAwesomeIcon icon={faLinkedinIn} />
	const instagram = <FontAwesomeIcon icon={faInstagram}/>
    const gmail = <FontAwesomeIcon icon={faGoogle}/>
    return(
        <>
        <section id="about" className="about-section">
        <div className="container">
        <header className="text-center">
          <h2 className="title">About me</h2>
        </header>
        <div className="row">
        <div className="col-lg-3 mx-auto mb-5"><img src={Me} alt="This is me - IT worker" className="image rounded-circle img-fluid center" height={300} width={300}/></div>
        </div>
        <div className="row">
          <div  className="col-lg-6">
            <p>I am <strong className="text-dark">Ismael Garen. </strong>  Building my own career as <strong className="text-dark">Full Stack Web Developer </strong>because I enjoy creating website using different kinds of programming language. I was on college when I first realized that I love programming. The first project I created was Invetory System for our School. I was assigned as Team Leader and Web Designer/Developer from front-end to back-end. I used HTML, CSS, JavaScript and PHP to build the webiste. I graduated as BSIT and find out that I want to be a web developer, but because of lacking financially, my parents aren't able to work because of their age and sick that's why I had to find a job ASAP so I started as a Branch Coordinator. Worked for 1 year. When I resigned, a company offered me a job as a Technical Sales Associate that is not related to programming. But again, because I am just the one who is working that time, I accepted it. Worked for 9 months as an Technical Sales Associate but the pandemic came so the management decided to give me a break. I have rested on working for almost 2 months before my current company offered me a job as a Junior IT Staff/Web Developer. I accepted it and fortunately, my sister has graduated and finished her study and became a Certified Public Accountant. That's the time I started to think to pursue my dream of becoming a web developer. Now, I am studying full stack web development in Zuitt Coding Bootcamp, and study wherever I am continiously. I love programming that's why I wanted to build my career on creating website applications.</p>
          </div>
          <div  className="col-lg-6">
            <div className="skill-item">
              <div className="progress-title">PHP</div>
              <div className="progress">
                <div role="progressbar" aria-valuenow="50" aria-valuemin="50" aria-valuemax="100" className="progress-bar progress-bar-skill1"></div>
              </div>
            </div>
            <div className="skill-item">
              <div className="progress-title">Javascript</div>
              <div className="progress">
                <div role="progressbar" aria-valuenow="60" aria-valuemin="60" aria-valuemax="100" className="progress-bar progress-bar-skill2"></div>
              </div>
            </div>
            <div className="skill-item">
              <div className="progress-title">HTML coding</div>
              <div className="progress">
                <div role="progressbar" aria-valuenow="70" aria-valuemin="80" aria-valuemax="100" className="progress-bar progress-bar-skill3"></div>
              </div>
            </div>
            <div className="skill-item">
              <div className="progress-title">CSS</div>
              <div className="progress">
                <div role="progressbar"  aria-valuenow="70" aria-valuemin="0" aria-valuemax="0" className="progress-bar progress-bar-skill4"></div>
              </div>
            </div>
            <div className="skill-item">
              <div className="progress-title">NextJS</div>
              <div className="progress">
                <div role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" className="progress-bar progress-bar-skill5"></div>
              </div>
            </div>
            <div className="skill-item">
              <div className="progress-title">ReactJS</div>
              <div className="progress">
                <div role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" className="progress-bar progress-bar-skill6"></div>
              </div>
            </div>
            <div className="skill-item">
              <div className="progress-title">Express JS</div>
              <div className="progress">
                <div role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" className="progress-bar progress-bar-skill7"></div>
              </div>
            </div>
            <div className="skill-item">
              <div className="progress-title">MongDB</div>
              <div className="progress">
                <div role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" className="progress-bar progress-bar-skill8"></div>
              </div>
            </div>
            <div className="skill-item">
              <div className="progress-title">SASS</div>
              <div className="progress">
                <div role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" className="progress-bar progress-bar-skill9"></div>
              </div>
            </div>
          </div>
          
        </div>
      </div>
    </section>
    
    <footer className="main-footer">
        <div className="container">
          <div className="row">
            <div className="col-md-6 text-center text-lg-left">
              <p className="social">
                <Link href="https://www.facebook.com/jovygaren/">  
                <a className="external facebook">{facebook}</a>
                </Link>
                <Link href="https://www.instagram.com/ismaelgrn/">
                <a className="external instagram">{instagram}</a>
                </Link>
                <Link href="https://www.gmail.com">
                <a className="external gplus">{gmail}</a>
                </Link>
                <Link href="https://www.linkedin.com/in/ismael-garen-3377711b9/">
                <a className="external email">{linkedIn}</a>
                </Link>
              </p>
            </div>

            <div className="col-md-6 text-center text-lg-right mt-4 mt-lg-0">
              <p>© 2021 All rights reserved.</p>
            </div>
            <div className="col-12 mt-4">
              <p className="template-bootstrapious">Template by <a href='https://www.facebook.com/jovygaren/'>Ismael Garen</a></p>
    
            </div>
          </div>
        </div>
    </footer>
    </>
    )
}