import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFacebookF } from '@fortawesome/free-brands-svg-icons';
import { faLinkedinIn } from '@fortawesome/free-brands-svg-icons';
import { faInstagram } from '@fortawesome/free-brands-svg-icons';
import { faGoogle } from '@fortawesome/free-brands-svg-icons';
import { faPhp } from '@fortawesome/free-brands-svg-icons';
import { faHtml5 } from '@fortawesome/free-brands-svg-icons';
import { faCss3 } from '@fortawesome/free-brands-svg-icons';
import { faJsSquare } from '@fortawesome/free-brands-svg-icons';
import { faNodeJs } from '@fortawesome/free-brands-svg-icons';
import { faReact } from '@fortawesome/free-brands-svg-icons';
import Link from 'next/link'

export default function Skills(){

    const facebook = <FontAwesomeIcon icon={faFacebookF} />
	const linkedIn = <FontAwesomeIcon icon={faLinkedinIn} />
	const instagram = <FontAwesomeIcon icon={faInstagram}/>
    const gmail = <FontAwesomeIcon icon={faGoogle}/>
    const php = <FontAwesomeIcon icon={faPhp}/>
    const html = <FontAwesomeIcon icon={faHtml5}/>
    const css = <FontAwesomeIcon icon={faCss3}/>
    const js = <FontAwesomeIcon icon={faJsSquare}/>
    const react = <FontAwesomeIcon icon={faReact}/>
    const next = <FontAwesomeIcon icon={faNodeJs}/>

    return(
        <>
        <section id="services" class="bg-gradient services-section">
            <div class="container">
                <header class="text-center">
                <h2 class="title">Skills</h2>
                </header>
                <div class="row services text-center">
                <div class="col-lg-4">
                    <div class="icon">{php}</div>
                    <h3 class="heading mb-3 text-400">PHP</h3>
                    <p class="text-left description">PHP is a general-purpose scripting language especially suited to web development. It was originally created by Danish-Canadian programmer Rasmus Lerdorf in 1994. The PHP reference implementation is now produced by The PHP Group.</p>
                </div>
                <div class="col-lg-4">
                    <div class="icon">{html}</div>
                    <h3 class="heading mb-3 text-400">HTML Coding</h3>
                    <p class="text-left description">Hypertext Markup Language is the standard markup language for documents designed to be displayed in a web browser. It can be assisted by technologies such as Cascading Style Sheets and scripting languages such as JavaScript.</p>
                </div>
                <div class="col-lg-4">
                    <div class="icon">{css}</div>
                    <h3 class="heading mb-3 text-400">CSS</h3>
                    <p class="text-left description">Cascading Style Sheets is a style sheet language used for describing the presentation of a document written in a markup language such as HTML. CSS is a cornerstone technology of the World Wide Web, alongside HTML and JavaScript.</p>
                </div>
                <div class="col-lg-4">
                    <div class="icon">{js}</div>
                    <h3 class="heading mb-3 text-400">JavaScript</h3>
                    <p class="text-left description">JavaScript, often abbreviated as JS, is a programming language that conforms to the ECMAScript specification. JavaScript is high-level, often just-in-time compiled, and multi-paradigm. It has curly-bracket syntax, dynamic typing, prototype-based object-orientation, and first-class functions.</p>
                </div>
                <div class="col-lg-4">
                    <div class="icon">{next}</div>
                    <h3 class="heading mb-3 text-400">NextJS</h3>
                    <p class="text-left description">Next.js is an open-source React front-end development web framework that enables functionality such as server-side rendering and generating static websites for React based web applications.</p>
                </div>
                <div class="col-lg-4">
                    <div class="icon">{react}</div>
                    <h3 class="heading mb-3 text-400">React</h3>
                    <p class="text-left description">React is an open-source, front end, JavaScript library for building user interfaces or UI components. It is maintained by Facebook and a community of individual developers and companies. React can be used as a base in the development of single-page or mobile applications.</p>
                </div>
                </div>
                <hr/>
                <div class="text-center">
                <p class="lead">Would you like to know more or just discuss something?</p>
                <p>
                    <Link href="/contact">
                    <a class="btn btn-outline-light link-scroll">Contact me</a>
                    </Link>
                </p>
                </div>
            </div>
        </section>

        <footer className="main-footer">
            <div className="container">
            <div className="row">
                <div className="col-md-6 text-center text-lg-left">
                <p className="social">
                    <Link href="https://www.facebook.com/jovygaren/">  
                    <a className="external facebook">{facebook}</a>
                    </Link>
                    <Link href="https://www.instagram.com/ismaelgrn/">
                    <a className="external instagram">{instagram}</a>
                    </Link>
                    <Link href="https://www.gmail.com">
                    <a className="external gplus">{gmail}</a>
                    </Link>
                    <Link href="https://www.linkedin.com/in/ismael-garen-3377711b9/">
                    <a className="external email">{linkedIn}</a>
                    </Link>
                </p>
                </div>

                <div className="col-md-6 text-center text-lg-right mt-4 mt-lg-0">
                <p>© 2021 All rights reserved.</p>
                </div>
                <div className="col-12 mt-4">
                <p className="template-bootstrapious">Template by <a href='https://www.facebook.com/jovygaren/'>Ismael Garen</a></p>
        
                </div>
            </div>
            </div>
        </footer>
        </>
    )
}