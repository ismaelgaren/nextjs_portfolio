import Head from 'next/head';
import Link from 'next/link';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFacebookF } from '@fortawesome/free-brands-svg-icons';
import { faTwitter } from '@fortawesome/free-brands-svg-icons';
import { faLinkedinIn } from '@fortawesome/free-brands-svg-icons';
import { faInstagram } from '@fortawesome/free-brands-svg-icons';
import { faGoogle } from '@fortawesome/free-brands-svg-icons';

export default function Home() {

  const facebook = <FontAwesomeIcon icon={faFacebookF} />
	const linkedIn = <FontAwesomeIcon icon={faLinkedinIn} />
	const instagram = <FontAwesomeIcon icon={faInstagram}/>
  const gmail = <FontAwesomeIcon icon={faGoogle}/>

  return (
    <>
      <Head>
        <meta charSet="utf-8" />
        <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
        <meta name="description" content="" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="robots" content="all,follow" />
    
        <title>Next Portfolio</title>
      </Head>

      <section id="intro" className="intro-section pb-2">
        <div className="container">
          <h1 className="text-shadow display-3">Hello, hola, नमस्ते !</h1>
          <h2 className="h3 text-shadow text-400 display-4">I am <span id="front">Ismael.</span></h2>
        </div>
      </section>

      <footer className="main-footer">
        <div className="container">
          <div className="row">
            <div className="col-md-6 text-center text-lg-left">
              <p className="social">
                <Link href="https://www.facebook.com/jovygaren/">  
                <a className="external facebook">{facebook}</a>
                </Link>
                <Link href="https://www.instagram.com/ismaelgrn/">
                <a className="external instagram">{instagram}</a>
                </Link>
                <Link href="https://www.gmail.com">
                <a className="external gplus">{gmail}</a>
                </Link>
                <Link href="https://www.linkedin.com/in/ismael-garen-3377711b9/">
                <a className="external email">{linkedIn}</a>
                </Link>
              </p>
            </div>

            <div className="col-md-6 text-center text-lg-right mt-4 mt-lg-0">
              <p>© 2021 All rights reserved.</p>
            </div>
            <div className="col-12 mt-4">
              <p className="template-bootstrapious">Template by <a href='https://www.facebook.com/jovygaren/'>Ismael Garen</a></p>
    
            </div>
          </div>
        </div>
      </footer>
    </>
  )
}
